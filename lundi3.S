#include <sys/syscall.h>
#include <asm/errno.h>

#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

#define STR2(a, b) ((a) | ((b) << 8))
#define STR3(a, b, c) (STR2(a, b) | (c) << 16)
#define STR4(a, b, c, d) (STR2(a, b) | (STR2(c, d) << 16))
#define STR5(a, b, c, d, e) (STR4(a, b, c, d) | ((e) << 32))
#define STR6(a, b, c, d, e, f) (STR4(a, b, c, d) | (STR2(e, f) << 32))
#define STR8(a, b, c, d, e, f, g, h) (STR4(a, b, c, d) | (STR4(e, f, g, h) << 32))

.text
.code64

.type syscall, STT_FUNC
/* int syscall(int nr : %rax, a1 : %rdi, a2 : %rsi, a3 : %rdx, %a4 : r10, %a5 : r8, %a6 : r9) : %rax */
syscall:
	syscall
	ret
.size syscall, .-syscall

.type read, STT_FUNC
/* ssize_t read(int fd : %edi, void *buf : %rsi, size_t count : %edx) : %eax */
read:
	movq $SYS_read, %rax
	jmp syscall
.size read, .-read

.type write, STT_FUNC
/* ssize_t write(int fd : %edi, const void *buf : %rsi, size_t count : %edx) : %eax */
write:
	movq $SYS_write, %rax
	jmp syscall
.size write, .-write

.type exit, STT_FUNC
/* void exit(int status : %edi) */
exit:
	movq $SYS_exit, %rax
	jmp syscall
.size exit, .-exit


.type _start, STT_FUNC
.global _start
_start:
	movq $stack_top, %rsp
	movq %rsp, %rbp
	call main

	movl %eax, %edi
	call exit
	jmp .
.size _start, .-_start


.type atoll, STT_FUNC
/* uint64_t atoll(char *str : %rdi, uint64_t len : %rsi) : %rax */
atoll:
	xorq %rax, %rax
	movq $1, %rcx
	addq %rdi, %rsi
1:
	cmpq %rsi, %rdi
	je 2f
	decq %rsi
	xorq %rdx, %rdx
	movb (%rsi), %dl
	subb $'0', %dl
	imulq %rcx, %rdx
	addq %rdx, %rax
	imulq $10, %rcx
	jmp 1b
2:
	ret
.size atoll, .-atoll

.type lltoa, STT_FUNC
/* void lltoa(uint64_t val : %rdi, char *str : %rsi) */
lltoa:
	movq $10000000000000000000, %rcx
	xorq %r9, %r9
1:
	xorq %rdx, %rdx
	movq %rdi, %rax
	divq %rcx
	movq %rdx, %rdi
2:
	cmpb $0, %r9b
	cmoveq %rax, %r9
	addb $'0', %al
	movb %al, (%rsi)
	cmpb $0, %r9b
	je 3f
	inc %rsi

3:
	xorq %rdx, %rdx
	movq %rcx, %rax
	movq $10, %r8
	divq %r8
	cmpq $0, %rax
	je 4f
	movq %rax, %rcx
	jmp 1b
4:
	movb $0, (%rsi)
	ret
.size lltoa, .-lltoa

.type strlen, STT_FUNC
/* size_t strlen(char *str : %rdi) */
strlen:
	movq %rdi, %rsi
	movq $-1, %rcx
	movb $0, %al
	cld
	repne scasb
	subq %rsi, %rdi
	movq %rdi, %rax
	ret
.size strlen, .-strlen


.type main, STT_FUNC
main:
1:
	movl $STDIN_FILENO, %edi
	movq $buf, %rsi
	movl $50, %edx
	call read
	cmpl $1, %eax
	jbe 5f
	movq $buf, %rdi
	leaq -1(%rax), %rsi
	call atoll

	xorq %rdx, %rdx
	cmp $1, %rax
	cmoveq %rax, %rcx
	je 3f
	cmp $0, %rax
	je 4f

	movq $2, %rcx
2:
	xorq %rdx, %rdx
	divq %rcx
	cmpq $1, %rax
	jle 3f
	cmpq $0, %rdx
	jne 4f
	incq %rcx
	jmp 2b

3:
	cmpq $0, %rdx
	jne 4f
	movw $STR2('=', ' '), buf
	movq %rcx, %rdi
	movq $buf + 2, %rsi
	call lltoa
	movq $buf, %rdi
	call strlen
	movl $STR3('!', '\n', 0), buf(%rax)
	addq $2, %rax
	movl $STDOUT_FILENO, %edi
	movq $buf, %rsi
	movq %rax, %rdx
	call write
	jmp 1b

4:
	movl $STDOUT_FILENO, %edi
	movq $msg_nofact, %rsi
	movq $msg_nofact.end - msg_nofact, %rdx
	call write
	jmp 1b

5:
	movl $0, %eax
	ret
.size main, .-main

.data

msg_nofact:
.asciz "Aucune factorielle possible.\n"
msg_nofact.end:

.bss

stack:
.skip 0x1000
stack_top:

buf: .skip 50
.size buf, .-buf

